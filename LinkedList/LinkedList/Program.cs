﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList20
{
    public class Node
    {
        public int Value { get; set; }
        public  Node Next { get; set; }
    }
    class LinkedList
    {
        Node Current;
        Node First;
        Node Last;
        public LinkedList(int value)
        {
            Node node = new Node();
            node.Value = value;
            node.Next = Current;
            Current = First;
            First = node;
            Last = node;
        }
        public void AddFirst(int value)
        {
            Node newnode = new Node();
            newnode.Value = value;
            Current = First;
            newnode.Next = Current;
            First = newnode;
            
        }
        public void AddLast(int value)
        {
            Node newNode = new Node();
            newNode.Value = value;
            Current = Last;
            Current.Next = newNode;
            Last = newNode;
        }
        public void AddAfter(int value, int valuetoadd)
        {
            Node temp;
            Node node = new Node();
            node.Value = valuetoadd;
            Current = First;
            while(Current.Value != value)
            {
                Current = Current.Next;  
            }
            temp = Current.Next;
            Current.Next = node;
            node.Next = temp;
        }
        public void AddBefore(int value, int valueToAdd)
        {
            Node Temp;
            Node BefTemp;
            Node node = new Node();
            node.Value = valueToAdd;
            Current = First;
            while(Current.Next.Value != value)
            {
                Current = Current.Next;
            }
            Temp = Current.Next;
            Current.Next = node;
            Current.Next.Next = Temp;
        }
        public Node Find(int value)
        {
            Current = First;
            while (Current.Value != value)
            {
                Current = Current.Next;
            }
            return Current;
        }
        public void Remove(int value)
        {
            Current = First;
            while(Current.Next.Value != value)
            {
                Current = Current.Next;
            } 
            Current.Next = Current.Next.Next;
        }
        public void RemoveFirst()
        {
            First = First.Next;
        }
        public void RemoveLast()
        {
            Current = First;
            while(Current.Next.Next != null)
            {
                Current = Current.Next;
            }
            Current.Next = null;
            Last = Current;
         }
        public int[] CopyTo(int [] arr)
        {
            int i = 0;
            Current = First;
            while(Current != null)
            {
                arr[i] = Current.Value;
                ++i;
                Current = Current.Next;
            }
            return arr;
        }
        public void PrintList()
        {
            Current = First;
            while(Current != null)
            {
                Console.WriteLine(Current.Value);
                Current = Current.Next;
            }

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList llist = new LinkedList(12);
            llist.AddFirst(10);
            llist.AddLast(60);
            llist.AddAfter(12,20);
            llist.AddBefore(60, 50);
            //llist.RemoveFirst();
           // llist.RemoveLast();
            //  Node nNode = llist.Find(20);
            ////llist.Remove(50);
            //llist.Remove(12);
             llist.PrintList();
            Console.ReadKey();
        }
    }
}
